include "hardware.inc"
include "header.asm"
include "structs.inc"
include "utils.asm"
include "vars.asm"

    struct Player
    bytes 1, XPos               ; 1 byte
    bytes 1, YPos               ; 1 byte
    bytes 1, LeftTileNumber     ; 1 byte
    bytes 1, RightTileNumber    ; 1 byte
    bytes 1, Flags              ; 1 byte
    bytes 1, Walking            ; 1 byte
    bytes 1, WalkingTimer       ; 1 byte
    end_struct

include "player.asm"


section "Game Code", rom0

START:
    ei

;enable vblank interrupt
    ld  sp, $FFFE
    ld  a, IEF_VBLANK
    ld  [rIE], a

    call WAIT_VBLANK
    ld a, $0
    ldh [rLCDC], a
	ldh [rSTAT], a

    ; clear everything
    call CLEAR_MAP
    call CLEAR_OAM
    call CLEAR_RAM
    call DMA_COPY

    ld hl, _VRAM    ; ld Sprite into location 9000
    ld de, Sprite
    ld bc, SpriteEnd - Sprite
.copySprite
    ld a, [de]  ; grab 1 byte from source
    ld [hli], a ; Place it into [hl], inc hl
    inc de      ; next byte
    dec bc      ; dec count
    ld a, b     ; check if count is 0, since dec bc doesn't update zero flag
    or c
    jr nz, .copySprite

    ; init display registers
    ld a, %11100100     ; pallete
    ld [rBGP], a
    ld a, %00011110     ; pallete
    ld [rOBP0], a

    ; reset scroll
    xor a               ; ld a, 0
    ld [rSCY], a
    ld [rSCX], a

.initPlayer
    ld a, 20                   ; initial start position
    ld [Player1_YPos], a
    ld [Player1_XPos], a
    ld a, 0                    ; initial tile
    ld [Player1_LeftTileNumber], a
    ld [Player1_Walking], a
    add 2
    ld [Player1_RightTileNumber], a
    ld [Player1_Flags], a      ; initial flags (none)
    ld a, 20
    ld [Player1_WalkingTimer], a

    ld a, LCDCF_ON|LCDCF_BG9800|LCDCF_OBJ16|LCDCF_OBJON
    ld [rLCDC], a

MainLoop:
    call WAIT_VBLANK
    call ReadJoypad
    call MOVE_PLAYER
    call DrawPlayer
    call _HRAM

    jr MainLoop

.lockup
    jr .lockup


Section "Sprite", rom0
Sprite:
incbin "player-2.chr"
SpriteEnd
SpritePalette:
incbin "player.pal"
