Section "Player", rom0

MOVE_PLAYER:
    ld a, [pad_state]
    and %00001111
    jr z, .noMovement

    and P1F_3           ; down key
    call nz, MoveDown

    ld a, [pad_state]
    and P1F_2           ; up key
    call nz, MoveUp

    ld a, [pad_state]
    and P1F_1           ; left key
    call nz, MoveLeft

    ld a, [pad_state]
    and P1F_0           ; right key
    call nz, MoveRight
    ret

.noMovement
    ; turn off walking animation
    ld a, [Player1_Walking]
    res 0, a
    ld [Player1_Walking], a
    ret

; Move the Player down
MoveDown:
    ld a, 0
    ld [Player1_LeftTileNumber], a
    add 2
    ld [Player1_RightTileNumber], a
    ld a, 0
    ld [Player1_Flags], a

    ld a, [Player1_YPos]
    cp 143                 ; check if at the bottom of the screen
    ret z
    inc a
    ld [Player1_YPos], a

    ld a, [Player1_Walking]
    set 0, a
    ld [Player1_Walking], a     ; set walking to true
    ret

; Move the Player up
MoveUp:
    ld a, 8
    ld [Player1_LeftTileNumber], a
    add 2
    ld [Player1_RightTileNumber], a
    ld a, 0
    ld [Player1_Flags], a

    ld a, [Player1_YPos]
    cp 16                   ; check if at the top of the screen
    ret z
    dec a
    ld [Player1_YPos], a

    ld a, [Player1_Walking]
    set 0, a
    ld [Player1_Walking], a     ; set walking to true
    ret

; Move the Player right
MoveRight:
    ld a, 16
    ld [Player1_LeftTileNumber], a
    add 2
    ld [Player1_RightTileNumber], a
    ld a, 0
    ld [Player1_Flags], a

    ld a, [Player1_XPos]
    cp 152                 ; check if at the right most of the screen
    ret z
    inc a
    ld [Player1_XPos], a

    ld a, [Player1_Walking]
    set 0, a
    ld [Player1_Walking], a     ; set walking to true
    ret

; Move the Player left
MoveLeft:
    ld a, 18
    ld [Player1_LeftTileNumber], a
    sub 2
    ld [Player1_RightTileNumber], a
    ld a, OAMF_XFLIP
    ld [Player1_Flags], a

    ld a, [Player1_XPos]
    cp 8                    ; check if ata the left most of the screen
    ret z
    dec a
    ld [Player1_XPos], a

    ld a, [Player1_Walking]
    set 0, a
    ld [Player1_Walking], a     ; set walking to true
    ret

; Update the walking var
UpdateWalking:
    ld a, [Player1_Walking]             ; is the player moving?
    and %00000001
    ret z                              ; if no return

    ld a, [Player1_WalkingTimer]        ; check if we need to update the sprite
    dec a
    ld [Player1_WalkingTimer], a
    cp 0
    ret nz                               ; return if no need to update

.changeWalkingSprite
    ld a, 5                             ; tweak this to make the sprite change faster for walking
    ld [Player1_WalkingTimer], a

    ld a, [Player1_Walking]             ; are we currently changing the sprite due to walking?
    and %00000010
    jr nz, .revertWalking               ; if yes, revet

    ; otherwise turn on
    ld a, [Player1_Walking]
    set 1, a
    ld [Player1_Walking], a
    ret                                 ; updated, now return

.revertWalking
    ; otherwise turn on
    ld a, [Player1_Walking]
    res 1, a
    ld [Player1_Walking], a
    ret

; Check to see if the alt walking sprite should be used
CheckAltSprite:
    ld a, [Player1_Walking]             ; check if the alt sprite needs to be used
    and %00000010
    ret z                               ; if not, should already be ok, return

    ld a, [Player1_Walking]             ; now check if we're moving
    and %00000001
    ret z                               ; if not, we've probably already updated the sprite, don't need to do again

    ld a, [Player1_LeftTileNumber]          ; get the alt sprite for both left and right
    add 4
    ld [Player1_LeftTileNumber], a

    ld a, [Player1_RightTileNumber] 
    add 4
    ld [Player1_RightTileNumber], a
    ret

; Draw the player sprites to shadow OAMRAM
DrawPlayer:
    call UpdateWalking
    call CheckAltSprite
.draw
    ld hl, player_sprite

    ld a, [Player1_YPos]
    ld [hli], a
    ld a, [Player1_XPos]
    ld [hli], a
    ld a, [Player1_LeftTileNumber]
    ld [hli], a
    ld a, [Player1_Flags]
    ld [hli], a

    ld a, [Player1_YPos]
    ld [hli], a
    ld a, [Player1_XPos]
    add 8
    ld [hli], a
    ld a, [Player1_RightTileNumber]
    ld [hli], a
    ld a, [Player1_Flags]
    ld [hli], a

    ret

Section "Structs", wram0
    dstruct Player, Player1     ; create player1 info