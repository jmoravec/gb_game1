CLEAR_MAP:
    ld  hl,_SCRN0
    ld  bc,$400
    push hl

.clear_map_loop
    ;wait for hblank
    ld  hl, rSTAT
    bit 1, [hl]
    jr  nz, .clear_map_loop
    pop hl

    ld  a, $0
    ld  [hli], a
    push hl

    dec bc
    ld  a,b
    or  c
    jr  nz, .clear_map_loop
    pop hl
    ret

CLEAR_OAM:
    ld  hl, _OAMRAM
    ld  bc, $A0
.clear_oam_loop
    ld  a, $0
    ld  [hli], a
    dec bc
    ld  a, b
    or  c
    jr  nz, .clear_oam_loop
    ret

CLEAR_RAM:
    ld  hl, $C100
    ld  bc, $A0
.clear_ram_loop
    ld  a, $0
    ld  [hli], a
    dec bc
    ld  a, b
    or  c
    jr  nz, .clear_ram_loop
    ret

; Wait for vblank, uses hl
WAIT_VBLANK:
    ld  hl,vblank_flag
.wait_vblank_loop
    halt
    nop        ;Hardware bug
    ld  a,$0
    cp  [hl]
    jr  z, .wait_vblank_loop
    ld  [hl], a
    ret


; DMA copy util, see https://gbdev.gg8.se/wiki/articles/OAM_DMA_tutorial
DMA_COPY:
    ; load de with the HRAM destination address
    ld  de,$FF80
    rst $28

    ; the amount of data we want to copy into HRAM, $000D which is 13 bytes
    DB  $00,$0D

    ; this is the above DMA subroutine hand assembled, which is 13 bytes long
    DB  $F5, $3E, $C1, $EA, $46, $FF, $3E, $28, $3D, $20, $FD, $F1, $D9
    ret

; Read the Joypad state into [pad_state], (1 means button is pressed)
; Kills a register
ReadJoypad:
    ld a, P1F_5     ; get direction keys
    ld [rP1], a

    ; settle bounce
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]

    cpl                     ; 0 means button is pressed, so flip the bits to make it easier to read
    ld [pad_state], a
    ret